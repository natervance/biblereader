# Bible Reader

A simple tool for reading scripture on Linux.

## What it is:

A single paned window with the minimum functionality required to read the Word.

## What it is not:

Fully featured bible study software for research, cross-referencing, commentaries, and footnotes.
If you want these things, please try the Xiphos project, which served as inspiration to this one.

## How to install translations:

Download .zip compressed SWORD modules from [crosswire.org](http://crosswire.org/sword/modules/index.jsp).
Then, in Bible Reader, install using the menu on the right of the header bar.
For a manual installation, unzip the module to `~/.sword/` (this is all that the GUI installer does).

## WIP Note

This is a work in progress. There have been a few tagged semi-stable-ish points, but it is currently under heavy development.
