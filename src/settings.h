#include <string>

void settingsWrite(std::string key, std::string value);

std::string settingsRead(std::string key);

void settingsWriteInt(std::string key, int value);

int settingsReadInt(std::string key);

