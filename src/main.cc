/* main.cpp
 *
 * Copyright (C) 2018 Nathan Vance
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "readerview.h"
#include "header.h"
#include <gtkmm.h>

int main(int   argc,
         char *argv[])
{
    auto app = Gtk::Application::create(argc, argv, "org.homelinuxserver.vance.biblereader");

    Gtk::Window window;
    window.set_default_size(550, 400);
    auto theme = Gtk::IconTheme::get_default();
    //theme->append_search_path(std::string(DATADIR) + "/icons/hicolor/scalable/apps");
    // No clue what the size *should* be. The 100 is in magical units :/
    window.set_icon(theme->load_icon("org.homelinuxserver.vance.biblereader-symbolic", 100));

    ReaderView reader;

    window.add(reader);

    Header *header = new Header(&reader, &window);
    window.set_titlebar(*header);

    window.show_all_children();

    return app->run(window);
}
