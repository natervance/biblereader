#include "mods.h"
#include "header.h"
#include "readerview.h"
#include <sword/swmgr.h>
#include <sword/swmodule.h>
#include <sword/installmgr.h>
#include <sword/filemgr.h>
#include <iostream>
#include <thread>
#include <sys/types.h>
#include <sys/stat.h>

void Mods::preStatus(long totalBytes, long completedBytes, const char *mess)
{
    std::lock_guard<std::mutex> lock(progressMutex);
    printf("Total: %ld, Completed: %ld, Message: %s\n", totalBytes, completedBytes, mess);
    message = mess;
}

// We want: https://developer.gnome.org/gtkmm-tutorial/stable/sec-multithread-example.html.en
void Mods::statusUpdate(double dltotal, double dlnow)
{
    std::lock_guard<std::mutex> lock(progressMutex);
    if (!dltotal) {
        //printf("Received a 0 dltotal\n");
        dispatcher.emit();
        return;
    }
    fracDone = (dlnow / dltotal);
    //printf("Progress: %f\n", fracDone);
    dispatcher.emit();
}

void Mods::getStatus(double *fractionDone, std::string *mess, bool *isComplete) const {
    std::lock_guard<std::mutex> lock(progressMutex);
    *fractionDone = fracDone;
    *mess = message;
    *isComplete = complete;
}

void Mods::onNotify() {
    double fractionDone;
    std::string mess;
    bool isComplete;
    getStatus(&fractionDone, &mess, &isComplete);
    if(isComplete) {
        if(worker && worker->joinable()) {
            worker->join();
            header->reader->modsUpdated();
            header->updateButtons();
            header->updateMenus();
            endProgress();
        }
        delete worker;
        worker = nullptr;
    } else {
        showProgress(mess);
        if(fractionDone >= 0 && fractionDone <= 1) {
            progressBar.set_fraction(fractionDone);
        } else {
            progressBar.pulse();
        }
    }
}

void Mods::showProgress(std::string message) {
    progressDialog.set_message(message);
    progressDialog.show();
    progressDialog.show_all_children();
}

void Mods::endProgress() {
    progressDialog.hide();
    return;
}

Mods::Mods(Header *header, Gtk::Window *window) : modsAvailable(), progressDialog("", false, Gtk::MessageType::MESSAGE_INFO, Gtk::ButtonsType::BUTTONS_CANCEL), progressBar(), dispatcher(), progressMutex(), worker(nullptr) {
    this->header = header;
    this->window = window;
    dispatcher.connect(sigc::mem_fun(*this, &Mods::onNotify));
    basedir = (g_getenv("HOME")) + std::string("/.sword/");
    mkdir((basedir + std::string("mods.d/")).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir((basedir + std::string("modules/")).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    confpath = basedir + std::string("InstallMgr/InstallMgr.conf");
    installMgr = new sword::InstallMgr((basedir + std::string("InstallMgr")).c_str(), this);
    progressDialog.signal_response().connect([this](int response) {
        installMgr->terminate();
    });
    progressDialog.get_message_area()->add(progressBar);
    progressBar.show();
    displayMain();
}

Mods::~Mods() {}

void Mods::displayMain() {
    remove();
    auto *scroll = Gtk::manage(new Gtk::ScrolledWindow);
    add(*scroll);
    auto *vbox = Gtk::manage(new Gtk::VBox);
    scroll->add(*vbox);
    auto *label = Gtk::manage(new Gtk::Label);
    label->set_text("Install new mods");
    vbox->pack_start(*label, false, false);
    auto *hbox = Gtk::manage(new Gtk::HBox);
    vbox->pack_start(*hbox, false, false);
    auto *network = Gtk::manage(new Gtk::VBox);
    hbox->add(*network);
    label = Gtk::manage(new Gtk::Label);
    label->set_text("Download over the network");
    network->pack_start(*label, false, false);
    auto *button = Gtk::manage(new Gtk::Button("Download"));
    network->pack_start(*button, false, false);
    button->signal_clicked().connect([this]() {
        if(installMgr->sources.size() > 1) {
            // The user previously agreed to the dialog
            installMgr->setUserDisclaimerConfirmed(true);
        }
        if(! installMgr->isUserDisclaimerConfirmed()) {
            auto *prompt = Gtk::manage(new Gtk::MessageDialog("This operation connects to known Christian affiliated web sites. Are you sure you wish to proceed?", false, Gtk::MessageType::MESSAGE_QUESTION, Gtk::ButtonsType::BUTTONS_OK_CANCEL, true));
            auto response = prompt->run();
            prompt->close();
            if (response == Gtk::RESPONSE_OK) {
                installMgr->setUserDisclaimerConfirmed(true);
            }
        }
        update();
    });
    auto *local = Gtk::manage(new Gtk::VBox);
    hbox->add(*local);
    label = Gtk::manage(new Gtk::Label);
    label->set_text("Install local .zip file");
    local->pack_start(*label, false, false);
    Gtk::FileChooserButton *add = Gtk::manage(new Gtk::FileChooserButton("Install SWORD modules", Gtk::FILE_CHOOSER_ACTION_OPEN));
    local->pack_start(*add, false, false);
    auto filter = Gtk::FileFilter::create();
    filter->add_mime_type("application/zip");
    add->set_filter(filter);
    add->signal_file_set().connect([this, add]() {
        installMods(add->get_filenames());
        auto newMods = this->header->reader->modsUpdated();
        if(! newMods.empty()) {
            this->header->reader->setVersion(newMods[0]);
        }
        this->header->updateMenus();
        this->header->updateButtons();
        this->header->showText();
    });
    window->show_all_children();
}

void Mods::displayDownload() {
    remove();
    auto *scroll = Gtk::manage(new Gtk::ScrolledWindow);
    add(*scroll);
    auto *vbox = Gtk::manage(new Gtk::VBox);
    //vbox->set_homogeneous(false);
    //vbox->set_hexpand(false);
    scroll->add(*vbox);
    auto *button = Gtk::manage(new Gtk::Button("Back"));
    vbox->pack_start(*button, false, false);
    button->signal_clicked().connect([this]() {
        displayMain();
    });
    auto *label = Gtk::manage(new Gtk::Label);
    label->set_text("Language Selection:");
    vbox->pack_start(*label, false, false);
    auto *langMenuButton = Gtk::manage(new Gtk::MenuButton);
    // Perhaps default to English? Or if translations for the app are added, default to the global language setting?
    langMenuButton->set_label("Select Language");
    vbox->pack_start(*langMenuButton, false, false);
    label = Gtk::manage(new Gtk::Label);
    label->set_text("Mods Available:");
    vbox->pack_start(*label, false, false);
    auto *modsSelection = Gtk::manage(new Gtk::VBox);
    modsSelection->set_vexpand(true);
    vbox->pack_end(*modsSelection);
    auto *langMenu = Gtk::manage(new Gtk::PopoverMenu);
    auto *sw = Gtk::manage(new Gtk::ScrolledWindow);
    langMenu->add(*sw);
    auto *langBox = Gtk::manage(new Gtk::VBox);
    sw->add(*langBox);
    sw->set_propagate_natural_width(true);
    sw->set_min_content_height(300);
    for(auto item : modsAvailable) {
        auto language = item.first;
        auto *langButton = Gtk::manage(new Gtk::Button(language));
        langButton->set_relief(Gtk::ReliefStyle::RELIEF_NONE);
        langButton->signal_clicked().connect([language, langMenuButton, langMenu, modsSelection, this]() {
            langMenu->popdown();
            langMenuButton->set_label(language);
            for(auto child : modsSelection->get_children()) {
                modsSelection->remove(*child);
            }
            for(auto curModPair : modsAvailable[language]) {
                auto *installButton = Gtk::manage(new Gtk::Button(curModPair.second->getName()));
                installButton->signal_clicked().connect([curModPair, this]() {
                    worker = new std::thread([curModPair, this] {
                        complete = false;
                        sword::SWMgr mgr(basedir.c_str());
                        installMgr->installModule(&mgr, 0, curModPair.second->getName(), curModPair.first);
                        complete = true;
                        dispatcher.emit();
                        header->showText();
                    });
                });
                modsSelection->pack_start(*installButton, false, false);
            }
            modsSelection->show_all_children();
        });
        langBox->add(*langButton);
    }
    langMenuButton->set_popover(*langMenu);
    langMenu->show_all_children();
    window->show_all_children();
}

void Mods::installMods(std::vector<std::string> filenames) {
    // So... turns out it's a mite unsupported to install from a .zip
    // Here's the deal. We do a syscall to unzip. We fancy like that.
    for(auto filename : filenames) {
        std::string command = "unzip -o " + filename + " -d " + basedir;
        if(system(command.c_str())) {
            //Uh oh...
            printf("Something bad happened when unpacking %s\n", filename);
        }
    }
}

void Mods::uninstallMods(std::vector<std::string> modnames) {
    sword::SWMgr mgr(basedir.c_str());
    for(auto mod : modnames) {
        sword::ModMap::iterator it = mgr.Modules.find(mod.c_str());
        installMgr->removeModule(&mgr, it->second->getName());
    }
}

void Mods::updateInstallable() {
    if(! modsAvailable.empty()) {
        return;
    }
    printf("Getting langs...\n");
    for(auto src : installMgr->sources) {
        if(src.second->getMgr()->Modules.empty()) {
            printf("Refreshing remote source: %s\n", src.second->getConfEnt().c_str());
            installMgr->refreshRemoteSource(src.second);
        }
        for(auto mod : src.second->getMgr()->Modules) {
            auto *curMod = mod.second;
            std::string type(curMod->getType());
            if(type == "Biblical Texts") {
                std::string language(curMod->getLanguage());
                //printf("Got language %s\n", language.c_str());
                std::vector<std::pair<sword::InstallSource *, sword::SWModule *>> newMods;
                // emplace only adds if key is unique
                modsAvailable.emplace(language, newMods);
                std::pair<sword::InstallSource *, sword::SWModule *> p(src.second, curMod);
                modsAvailable[language].push_back(p);
            }
        }
    }
}

void Mods::update() {
    if(! sword::FileMgr::existsFile(confpath.c_str())) {
        // Lifted directly from xiphos
        sword::FileMgr::createParent(confpath.c_str());
        sword::SWConfig config(confpath.c_str());
        sword::InstallSource is("FTP");
        is.caption = "CrossWire";
        is.source = "ftp.crosswire.org";
        is.directory = "/pub/sword/raw";
        config["General"]["PassiveFTP"] = "true";
        config["Sources"]["FTPSource"] = is.getConfEnt();
        config.save();
    }
    installMgr->readInstallConf();
    worker = new std::thread([this] {
        complete = false;
        preStatus(-1, -1, "Refreshing remote sources...");
        statusUpdate(1, -1);
        dispatcher.emit();
        installMgr->refreshRemoteSourceConfiguration();
        updateInstallable();
        complete = true;
        dispatcher.emit();
        if(installMgr->sources.size() > 1) {
            displayDownload();
        }
    });
}
